package com.example.trivia.model

data class Option(val category: String, val question: String, val possibleOption1: String, val possibleOption2: String, val possibleOption3: String, val correctOption: String)
