package com.example.trivia.model

class Options {
    companion object{
        val options = mutableListOf<Option>(
            //1
            Option("Historia","¿En qué año el hombre pisó la Luna por primera vez?","2001","1969","1987", "1969"),
            Option("Historia","¿Por cultivar qué producto es conocido Guatemala?","Té","Chocolate","Café", "Café"),
            Option("Historia","¿Cómo se llama la Reina del Reino Unido?","Encarni","Maria","Isabel", "Isabel"),
            Option("Historia","¿En qué año comenzó la II Guerra Mundial?","1939","1942","1937", "1939"),
            Option("Historia","¿Cuándo empezó la Primera Guerra Mundial?","1914","1908","1929", "1914"),
            Option("Historia","¿Cómo se llama el himno nacional de Francia?","La Vie Rose","Désenxanté","La Marsellesa", "La Marsellesa"),
            Option("Historia","¿Cuál fue el primer metal que empleó el hombre?","Hierro","Cobre","Estaño", "Cobre"),
            Option("Historia","¿En qué año el hombre pisó la Luna por primera vez?","2001","1969","1987", "1969"),

            //2
            Option("Esports","¿En qué ciudad española está el estadio de fútbol de Mestalla?","Madrid","Barcelona","Valencia", "Valencia"),
            Option("Esports","¿Qué deporte jugado con un bate y una pelota es el más popular en la India?","Críquet","Bádminton","Béisbol ", "Críquet"),
            Option("Esports","¿De dónde son originarios juegos olímpicos?","Italia","Grecia","Francia ", "Grecia"),
            Option("Esports","¿Qué deporte practicaba Carl Lewis?","Halterofilia","Atletismo","Golf ", "Atletismo"),
            Option("Esports","¿Qué selección de fútbol ha ganado más Mundiales de fútbol? ","Argentina","Brasil","España ", "Brasil"),
            Option("Esports","¿Quién ganó el mundial de fútbol de 2010?","Argentina","Brasil","Espana ", "Espana"),
            Option("Esports","¿Cuántos jugadores componen un equipo de rugby?","12","15","18 ", "15"),
            Option("Esports","¿Cuántos títulos de motociclismo ha conseguido Valentino Rossi?","9","12","16 ", "9"),

            //3
            Option("Entreteniment","¿Cómo se llama el antagonista principal de la película de Disney El Rey León?","Scar","Zazu","Mufasa", "Scar"),
            Option("Entreteniment","¿Cuál es el único mamífero capaz de volar?","Pinguino","Murciélago","Gallo", "Murciélago"),
            Option("Entreteniment","¿Cuántos dientes tiene una persona adulta?","26","32","46", "Scar"),
            Option("Entreteniment","¿Cuál es el libro sagrado de la religión musulmana?","Torá","Corán","Bíblia", "Corán"),
            Option("Entreteniment","Según los escritos bíblicos, ¿Quién traicionó a Jesús?","Judas","Juan","Rafael", "Judas"),
            Option("Entreteniment","¿Qué enfermedad padece Stephen Hawking?","AINE","ELA","EPOC", "ELA"),
            Option("Entreteniment","¿En qué lugar escribió Cervantes la mayor parte de El Quijote?","Su pueblo","La cárcel","El hospital", "La cárcel"),
            Option("Entreteniment","¿En qué año se estrenó la película Titanic?","1997","1999","1984", "1997"),

            //4
            Option("Geografia","¿Cuál es la capital de Filipinas?","Manila","Cebu","Davao", "Manila"),
            Option("Geografia","¿Ciudad más poblada mundo?","Delhi","Tokio","Cantón", "Yakarta"),
            Option("Geografia","¿Cuál es el río más largo de España?","Duero","Tajo","Ebro", "Tajo"),
            Option("Geografia","¿Cuál es el país más grande del mundo?","Rusia","EEUU","China", "Rusia"),
            Option("Geografia","¿Cuál es la moneda Marruecos?","Rupia","Dirham","Denar", "Dirham"),
            Option("Geografia","¿Cual es país más poblado de la Tierra?","Rusia","EEUU","China", "China"),
            Option("Geografia","¿Cuál es la capital de Kosovo?","Taixkent","Kosovo","Astanà", "Kosovo"),
            Option("Geografia","¿Cuál es la capital de Filipinas?","Manila","Cebu","Davao", "Manila"),

            //5
            Option("Ciencia","¿De qué se alimentan los koalas?","Eucalipto","Laurel","Cilantro", "Eucalipto"),
            Option("Ciencia","¿Cuántas veces parpadea una persona a la semana?","1200","5700","25000", "25000"),
            Option("Ciencia","¿Qué cantidad de huesos en el cuerpo humano adulto?","94","132","206", "206"),
            Option("Ciencia","¿La ballena qué tipo de animal es?","Mamífero","Ave","Pez", "Mamífero"),
            Option("Ciencia","¿Cuál es quinto planeta en el sistema solar?","Júpiter","Urano","Saturno", "Júpiter"),
            Option("Ciencia","¿Cuál es el metal más caro del mundo?","Platino","Rodio","Grafeno", "Rodio"),
            Option("Ciencia","¿En qué lugar del cuerpo se produce la insulina?","Páncreas","Riñones","Intestinos", "Páncreas"),
            Option("Ciencia","¿Qué rama de la Biología estudia los animales?","Zoología","Animalogia","Embriología", "zoología"),


            //6
            Option("Art","¿De qué país es originario el tipo de poesía conocido como haiku?","China","Corea","Japón", "Japón"),
            Option("Art","¿Quién escribió La Odisea?","Lenin","Shakespeare","Homero", "Homero"),
            Option("Art","¿Cuál es el área del arte protagonista en los premios Goya?","Libros","Películas","Canciones", "Películas"),
            Option("Art","¿De qué estilo arquitectónico es la Catedral de Notre Dame en París?","Gótico","Barroco","Contemporáneo", "Gótico"),
            Option("Art","¿Quién pronunció la frase “solo sé que no sé nada”?","Aristoteles","Sócrates","Platón", "Sócrates"),
            Option("Art","¿Quién es el autor de el Quijote?","Lorca","Machado","Cervantes", "Cervantes"),
            Option("Art","¿Quién diseño la Sagrada Familia?","Gaudí","Ábalos","Baeza", "Gaudí"),
            Option("Art","¿Cuál es el país de nacimiento de Pablo Neruda?","Bolivia","México","Chile", "Chile"),

            )
    }
}