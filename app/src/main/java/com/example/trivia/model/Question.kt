package com.example.trivia.model

data class Question(val category: String ,val question: String, val possibleAnswer1: String, val possibleAnswer2: String, val possibleAnswer3: String, val possibleAnswer4: String, val correctAnswer: String)
