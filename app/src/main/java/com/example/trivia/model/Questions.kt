package com.example.trivia.model

class Questions {
    companion object{
        val questions = mutableListOf<Question>(
            //1
            Question("Historia","¿Quién escribió la Ilíada y la Odisea?","Marqués de Sade","Charles Dickens","Oscar Wilde","Homero", "Homero"),
            Question("Historia","¿Quién fue el primer presidente de la democracia española tras el franquismo?","Adolfo Suárez","Jose Luís Zapatero","Pedro Sanchez","Pablo Iglesias", "Adolfo Suárez"),
            Question("Historia","¿En qué año el hombre pisó la Luna por primera vez?","2001","1969","1987","1948", "1969"),
            Question("Historia","Quien es el actual rey de españa?","Juan Carlos I","Francisco Franco II","Rober Widow","Felipe VI", "Felipe VI"),
            Question("Historia","¿Quiénes fueron, según la leyenda, los dos hermanos fundadores de la ciudad de Roma?","Rómulo y Remo","Kourtney y Kim Kardashian","Mario y Óscar Casas","Gaius y Manius", "Rómulo y Remo"),
            Question("Historia","¿Quién fue el primer presidente de Estados Unidos?","George Washington ","Donald Trump","Thomas Jefferson","John Hanson", "George Washington "),
            Question("Historia","¿Qué carabela no volvió del viaje en el que Colón arribó a América por primera vez?","La Pinta","La Santa María","La Niña","El Glorioso", "La Santa María"),
            Question("Historia","¿Cómo se apellidaban los dos exploradores que dieron la primera vuelta al mundo?","Hernando de Soto","Cristóbal Colón","Vasco Núñez de Balboa","Expedición Magallanes-Elcano", "Expedición Magallanes-Elcano"),
            //2
            Question("Esports","¿Qué atleta tiene el record plusmarca de velocidad en los 100 metros lisos?","Usain Bolt","Tyson Gay ","Justin Gatlin","Yohan Blake", "Usain Bolt"),
            Question("Esports","¿En qué ciudad española está el estadio de fútbol de Mestalla?","Madrid","Barcelona","Bilbao","Valencia", "Valencia"),
            Question("Esports","¿Qué deporte jugado con un bate y una pelota es el más popular en la India?","Críquet","Bádminton","Béisbol ","Hockey", "Críquet"),
            Question("Esports","¿Qué arte marcial es conocido como “el boxeo tailandés”?","Jiu-jitsu","Muay Thai","Krav Maga","Judo", "Muay Thai"),
            Question("Esports","¿Cuál es el tenista que ha ganado en más ocasiones el título de Roland Garros?","Rafael Nadal","Roger Federer","Carlos Alcaraz","Novak Dokovic", "Rafael Nadal"),
            Question("Esports","¿Qué conocido boxeador inició su trayectoria profesional con el nombre de Cassius Clay?","Myke Tyson","Rocky Marciano","Floyd Mayweather","Muhammad Ali", "Muhammad Ali"),
            Question("Esports","¿En qué franquicia de la NBA desarrolló toda su carrera profesional Kobe Bryant?","Boston Celtics","Angeles Lakers","Chicago Bulls","Dallas Mavericks", "Angeles Lakers"),
            Question("Esports","¿Qué jugador de la NBA ha ganado más campeonatos?","Bill Russell","Michael Jordan","Stephen Curry","LeBron James", "Bill Russell"),
            //3
            Question("Entreteniment","¿Cómo se llama el antagonista principal de la película de Disney El Rey León?","Simba","Scar","Zazu","Mufasa", "Scar"),
            Question("Entreteniment","¿Quién fue el famoso cantante del grupo musical Queen?","Freddie Mercury","Imagine Dragons","Coldplay","Oasis", "Freddie Mercury"),
            Question("Entreteniment","¿A qué banda de música metal pertenece el disco Master of Puppets?","Deep Purple","Guns N' Roses","Metallica","Iron Maiden", "Metallica"),
            Question("Entreteniment","¿De qué grupo es la canción “Smells like a teen spirit”?","Nirvana","Green River","Alice in Chains","Soundgarden", "Nirvana"),
            Question("Entreteniment","¿A quién interpretaba John Travolta en “Grease”?","Putzie","Danny Zuko","Sandy Olson","Sonny", "Danny Zuko"),
            Question("Entreteniment","¿Quién es el guionista de la novela gráfica Watchmen?","Alan Moore","Ben Hecht","Charlie Kaufman","Leigh Brackett", "Alan Moore"),
            Question("Entreteniment","¿Qué famoso dúo musical participó en la banda sonora de la película Tron: Legacy?","Backstreet Boys","Maroon 5","Daft Punk","Spice Girls", "Daft Punk"),
            Question("Entreteniment","Quien es el actual rey de españa?","Juan Carlos I","Francisco Franco II","Rober Widow","Felipe VI", "Felipe VI"),
            //4
            Question("Geografia","¿De qué país es originario el tipo de poesía conocido como haiku?","China","Corea","Japón","Filipinas", "Japón"),
            Question("Geografia","¿Cuál es la lengua más hablada del mundo?","Francés","Español","Chino mandarín","Inglés", "Chino mandarín"),
            Question("Geografia","¿Cuál es la capital de Brasil?","Sao Paulo","Brasilia","Rio de Janeiro","Salvador", "Brasilia"),
            Question("Geografia","¿Cuál es la capital de Filipinas?","Manila","Quezon","Cebu","Davao", "Manila"),
            Question("Geografia","¿En qué país se habla mayoritariamente el idioma tagálog?","Taiwán","China","Filipinas","Vietnam", "Filipinas"),
            Question("Geografia","¿Cuál es la ciudad más poblada de África?","El Cairo","Johannesburgo","Luanda","Nairobi", "El Cairo"),
            Question("Geografia","¿Cuál es el nombre de la capital de Albania?","Sofía","Tirana","Belgrado","Bucarest", "Tirana"),
            Question("Geografia","¿Qué montaña es uno de los símbolos nacionales de Armenia?","Urtsi Lerr","Arayi Lerr","Aragats","Monte Ararat", "Monte Ararat"),
            //5
            Question("Ciencia","¿De qué se alimentan los koalas?","Eucalipto","Laurel","Cilantro", "Romero","Eucalipto"),
            Question("Ciencia","¿Cuántas veces parpadea una persona a la semana?","900","1200","5700","25000", "25000"),
            Question("Ciencia","¿Qué cantidad de huesos en el cuerpo humano adulto?","94","132","206", "356","206"),
            Question("Ciencia","¿La ballena qué tipo de animal es?","Mamífero","Ave","Pez", "Crustáceo","Mamífero"),
            Question("Ciencia","¿Cuál es quinto planeta en el sistema solar?","Júpiter","Urano","Saturno","Mercurio" ,"Júpiter"),
            Question("Ciencia","¿Cuál es el metal más caro del mundo?","Platino","Rodio","Grafeno", "Oro","Rodio"),
            Question("Ciencia","¿En qué lugar del cuerpo se produce la insulina?","Páncreas","Riñones","Intestinos", "Pulmones","Páncreas"),
            Question("Ciencia","¿Qué rama de la Biología estudia los animales?","Zoología","Animalogia","Embriología","Cartología", "Zoología"),
            //6
            Question("Art","¿De qué personaje ficticio estaba enamorado el Quijote?","Dulcinea","Dorotea","Camila","Luscinda", "Dulcinea"),
            Question("Art","¿En qué otro idioma, además del castellano, escribió la novelista y poetisa Rosalía de Castro?","Gallego","Italiano","Catalan","Valenciano", "Gallego"),
            Question("Art","¿Cómo se llama el pintor noruego autor de la obra “El Grito”?","Pablo Picasso","Edvard Munch","Rembrandt","Claude Money", "Edvard Munch"),
            Question("Art","¿Qué escritor hispanoparlante recibió el apodo de “el manco de Lepanto”?","Charles Dickens","William Shakespeare","Stephen King","Miguel de Cervantes", "Miguel de Cervantes"),
            Question("Art","¿De qué país es originario el tipo de poesía conocido como haiku?","China","Corea","Japón","Filipinas", "Japón"),
            Question("Art","¿Quién escribió “La colmena”?","Lope de Vega","Antonio Machado","Camilo José Cela","Federico García Lorca", "Camilo José Cela"),
            Question("Art","¿Qué gran artista es conocido por haber pintado la Capilla Sixtina?","Vincent Van Gogh","Miguel Angel","Pablo Picasso","Peter Paul Rubens", "Miguel Angel"),
            Question("Art","¿Quién pintó el “Guernica”?","Edvard Munch","Caravaccio","Pablo Picasso","Joan Miró", "Pablo Picasso"),
            )
    }
}