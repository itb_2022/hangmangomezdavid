package com.example.trivia.model

data class User(var userName: String, var password: String, var points: Int)
