package com.example.trivia.view.View.Game

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import com.example.trivia.R
import com.example.trivia.databinding.FragmentGameBinding
import com.example.trivia.view.View.Help.FragmentHelp_How
import com.example.trivia.view.View.Menu.FragmentMenu


class FragmentGame : Fragment() {
    lateinit var binding: FragmentGameBinding
    var category = ""
    var rangeCategory = 0
    var rangeDurationCategory = 0
    var rangeFragment = 0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        binding = FragmentGameBinding.inflate(layoutInflater)
        return binding.root
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        var categoryList = mutableListOf<String>("Historia", "Esports", "Entreteniment", "Geografia", "Ciencia ", "Art")
        category = categoryList.random()
        categoryList.remove(category)
        println(categoryList)

        rangeCategory = (15..30).random()
        rangeDurationCategory = (2500..4000).random()

        binding.SpinBT.setOnClickListener {
            binding.SpinBT.isEnabled = false
            binding.SpinBT.isClickable = false
            binding.BackBT.isEnabled = false
            binding.BackBT.isClickable = false

            when (category){
                //45ºNext position + 22.5º(center category)
                "Historia" -> {
                    binding.roulette.animate().rotation(720f+rangeCategory).duration=rangeDurationCategory.toLong()
                    Handler().postDelayed({
                        binding.roulette.isVisible = false
                        binding.pointer.isVisible = false

                        binding.selectedCategory.setBackgroundResource(R.drawable.hector)
                        binding.categoryName.text = "$category"
                        binding.selectedCategory.isVisible = true
                        binding.categoryName.isVisible = true
                        binding.SpinBT.isVisible = false
                        binding.BackBT.isVisible = false
                    }, 4000)
                }
                "Esports" -> {
                    binding.roulette.animate().rotation(765f+rangeCategory).duration=rangeDurationCategory.toLong()
                    Handler().postDelayed({
                        binding.roulette.isVisible = false
                        binding.pointer.isVisible = false

                        binding.selectedCategory.setBackgroundResource(R.drawable.bonzo)
                        binding.categoryName.text = "$category"
                        binding.selectedCategory.isVisible = true
                        binding.categoryName.isVisible = true
                        binding.SpinBT.isVisible = false
                        binding.BackBT.isVisible = false
                    }, 4000)
                }
                "Entreteniment" -> {
                    binding.roulette.animate().rotation(810f+rangeCategory).duration=rangeDurationCategory.toLong()
                    Handler().postDelayed({
                        binding.roulette.isVisible = false
                        binding.pointer.isVisible = false

                        binding.selectedCategory.setBackgroundResource(R.drawable.pop)
                        binding.categoryName.text = "$category"
                        binding.selectedCategory.isVisible = true
                        binding.categoryName.isVisible = true
                        binding.SpinBT.isVisible = false
                        binding.BackBT.isVisible = false
                    }, 4000)
                }
                "Geografia" -> {
                    binding.roulette.animate().rotation(855f+rangeCategory).duration=rangeDurationCategory.toLong()
                    Handler().postDelayed({
                        binding.roulette.isVisible = false
                        binding.pointer.isVisible = false

                        binding.selectedCategory.setBackgroundResource(R.drawable.tito)
                        binding.categoryName.text = "$category"
                        binding.selectedCategory.isVisible = true
                        binding.categoryName.isVisible = true
                        binding.SpinBT.isVisible = false
                        binding.BackBT.isVisible = false
                    }, 4000)
                }
                "Ciencia" -> {
                    binding.roulette.animate().rotation(890f+rangeCategory).duration=rangeDurationCategory.toLong()
                    Handler().postDelayed({
                        binding.roulette.isVisible = false
                        binding.pointer.isVisible = false

                        binding.selectedCategory.setBackgroundResource(R.drawable.albert)
                        binding.categoryName.text = "$category"
                        binding.selectedCategory.isVisible = true
                        binding.categoryName.isVisible = true
                        binding.SpinBT.isVisible = false
                        binding.BackBT.isVisible = false
                    }, 4000)
                }
                "Art" -> {
                    binding.roulette.animate().rotation(935f+rangeCategory).duration=rangeDurationCategory.toLong()
                    Handler().postDelayed({
                        binding.roulette.isVisible = false
                        binding.pointer.isVisible = false

                        binding.selectedCategory.setBackgroundResource(R.drawable.tina)
                        binding.categoryName.text = "$category"
                        binding.selectedCategory.isVisible = true
                        binding.categoryName.isVisible = true
                        binding.SpinBT.isVisible = false
                        binding.BackBT.isVisible = false
                    }, 4000)
                }
            }

            rangeFragment = (1..3).random()
            println(rangeFragment)
            Handler(Looper.getMainLooper()).postDelayed({

                val bundle = Bundle()
                bundle.putString("category", category)

                parentFragmentManager.setFragmentResult(
                    "userData",
                    bundle
                )

                when(rangeFragment){
                    1 -> {
                        parentFragmentManager.beginTransaction().apply {
                            replace(R.id.fragmentContainerView, (FragmentGame_options()))
                            setReorderingAllowed(true)
                            commit()
                        }
                    }
                    2 -> {
                        parentFragmentManager.beginTransaction().apply {
                            replace(R.id.fragmentContainerView, (FragmentGame_options()))
                            setReorderingAllowed(true)
                            commit()
                        }
                    }
                    3 -> {
                        parentFragmentManager.beginTransaction().apply {
                            replace(R.id.fragmentContainerView, (FragmentGame_options()))
                            setReorderingAllowed(true)
                            commit()
                        }
                    }
                }
            },6000)

        }
        binding.BackBT.setOnClickListener {
            parentFragmentManager.beginTransaction().apply {
                replace(R.id.fragmentContainerView, FragmentMenu())
                setReorderingAllowed(true)
                commit()
            }
        }
      }
}