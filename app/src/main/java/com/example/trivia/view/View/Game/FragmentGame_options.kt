package com.example.trivia.view.View.Game

import android.annotation.SuppressLint
import android.app.Activity
import android.graphics.Color
import android.os.Bundle
import android.os.CountDownTimer
import android.os.Handler
import android.view.KeyEvent
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.os.HandlerCompat.postDelayed
import androidx.core.view.isVisible
import com.example.trivia.R
import com.example.trivia.databinding.FragmentGameOptionsBinding
import com.example.trivia.model.Options
import com.example.trivia.model.Questions
import com.example.trivia.view.View.Menu.FragmentMenu
import java.io.OutputStreamWriter

class FragmentGame_options : Fragment(), View.OnClickListener {
    lateinit var binding: FragmentGameOptionsBinding
    lateinit var timer: CountDownTimer
    var randomQuestion = 0
    var category = ""
    var result = ""
    var turnNumber = 0
    var points = 0
    var rangeFragment = 0


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): ConstraintLayout? {
        binding = FragmentGameOptionsBinding.inflate(layoutInflater)
        return binding.root
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        parentFragmentManager.setFragmentResultListener("userData", this,
            { requestKey: String, result: Bundle ->
                category = result.getString("category").toString()
            })


        Handler().postDelayed({
            var categoryList = mutableListOf<String>("Historia", "Esports", "Entreteniment", "Geografia", "Ciencia ", "Art")

            binding.categoryTextView.text = category
            randomQuestion = (0..7).random()
            binding.buttonA.setOnClickListener(this)
            binding.buttonB.setOnClickListener(this)
            binding.buttonC.setOnClickListener(this)
            binding.progressbarr.progress = 20

            generateQuestion(category, turnNumber)

        } , 0)

        timer = object : CountDownTimer(20000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                binding.progressbarr.progress--
                binding.progressSeconds.text = (" ${binding.progressbarr.progress.toString()} s")
            }
            override fun onFinish() {
                binding.buttonA.isClickable = false
                binding.buttonA.alpha = 0.5f
                binding.buttonB.isClickable = false
                binding.buttonB.alpha = 0.5f
                binding.buttonC.isClickable = false
                binding.buttonC.alpha = 0.5f
                Handler().postDelayed({
                randomQuestion = (0..7).random()
                    points -= 1
                    turnNumber += 1
                    binding.numberQuestion.text = "$turnNumber / 8 Preguntes"
                    when (turnNumber){
                        1 -> {
                            binding.buttonA.isVisible = true
                            binding.buttonB.isVisible = true
                            binding.buttonC.isVisible = true

                            binding.edittextAnswerthequestion.isVisible = false
                            binding.SendBT.isVisible = false
                        }
                        2 -> {
                            binding.buttonA.isVisible = true
                            binding.buttonB.isVisible = true
                            binding.buttonC.isVisible = true

                            binding.edittextAnswerthequestion.isVisible = false
                            binding.SendBT.isVisible = false
                        }
                        3 -> {
                            binding.buttonA.isVisible = true
                            binding.buttonB.isVisible = true
                            binding.buttonC.isVisible = true

                            binding.edittextAnswerthequestion.isVisible = false
                            binding.SendBT.isVisible = false
                        }
                        4 -> {
                            binding.buttonA.isVisible = false
                            binding.buttonB.isVisible = false
                            binding.buttonC.isVisible = false

                            binding.edittextAnswerthequestion.isVisible = true
                            binding.SendBT.isVisible = true
                        }
                        5 -> {
                            binding.buttonA.isVisible = true
                            binding.buttonB.isVisible = true
                            binding.buttonC.isVisible = true

                            binding.edittextAnswerthequestion.isVisible = false
                            binding.SendBT.isVisible = false
                        }
                        6 -> {
                            binding.buttonA.isVisible = false
                            binding.buttonB.isVisible = false
                            binding.buttonC.isVisible = false

                            binding.edittextAnswerthequestion.text.clear()
                            binding.edittextAnswerthequestion.isVisible = true
                            binding.SendBT.isVisible = true
                        }
                        7 -> {
                            binding.buttonA.isVisible = true
                            binding.buttonB.isVisible = true
                            binding.buttonC.isVisible = true

                            binding.edittextAnswerthequestion.isVisible = false
                            binding.SendBT.isVisible = false
                        }
                        8 -> {
                            binding.buttonA.isVisible = true
                            binding.buttonB.isVisible = true
                            binding.buttonC.isVisible = true

                            binding.edittextAnswerthequestion.isVisible = false
                            binding.SendBT.isVisible = false
                        }
                    }
                    if (turnNumber != 4 || turnNumber != 6)generateQuestion(category, turnNumber)
                    else if (turnNumber == 4 || turnNumber == 6)generateQuestionSecondOption(category, turnNumber)
                    binding.buttonA.isClickable = true
                    binding.buttonB.isClickable = true
                    binding.buttonC.isClickable = true
                    binding.buttonA.alpha = 1f
                    binding.buttonB.alpha = 1f
                    binding.buttonC.alpha = 1f

                    binding.buttonA.setBackgroundColor(resources.getColor(R.color.aquamarine))
                    binding.buttonB.setBackgroundColor(resources.getColor(R.color.aquamarine))
                    binding.buttonC.setBackgroundColor(resources.getColor(R.color.aquamarine))
                    binding.progressbarr.progress = 20
                    binding.progressSeconds.text = (" ${binding.progressbarr.progress.toString()} s")
                    timer.start()

                    if (turnNumber == 8){
                        timer.cancel()
                        Handler().postDelayed({
                            parentFragmentManager.beginTransaction().apply {
                                replace(R.id.fragmentContainerView, (FragmentGame()))
                                setReorderingAllowed(true)
                                commit()
                            }
                        },2500)
                    }
                } , 1000)
            }
        }.start()
    }

    @SuppressLint("SuspiciousIndentation")
    override fun onClick(p0: View?) {
        val button = p0 as Button
        val text = button.text.toString()
        //binding.progressbarr.cancelLongPress()

        timer.cancel()
        when(button){
            binding.buttonA -> {
                binding.buttonB.isClickable = false
                binding.buttonC.isClickable = false
            }
            binding.buttonB -> {
                binding.buttonA.isClickable = false
                binding.buttonC.isClickable = false
            }
            binding.buttonC -> {
                binding.buttonB.isClickable = false
                binding.buttonA.isClickable = false
            }

        }

        button.isEnabled = false
        button.background.alpha = 92

            if (text == result) {
                println("bien")
                button.setBackgroundColor(Color.GREEN)
                points += 10
                turnNumber += 1
                binding.numberQuestion.text = "$turnNumber / 8 Preguntes"
                println(points)
            } else if (text != result) {
                println("mal")
                button.setBackgroundColor(Color.DKGRAY)
                points -= 1
                turnNumber += 1
                binding.numberQuestion.text = "$turnNumber / 8 Preguntes"
                println(points)
            }

            binding.edittextAnswerthequestion.setOnKeyListener { view, keyCode, keyevent -> //If the keyevent is a key-down event on the "enter" button
                println(keyCode)
                println(keyevent.action)
                if (keyevent.action == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
                    println(keyCode)
                    println(keyevent.action)
                    if (binding.edittextAnswerthequestion.text.toString().uppercase() == result) {
                        points += 10
                        println("Points $points")
                        turnNumber += 1
                        when (turnNumber){
                            1 -> {
                                binding.buttonA.isVisible = true
                                binding.buttonB.isVisible = true
                                binding.buttonC.isVisible = true

                                binding.edittextAnswerthequestion.isVisible = false
                                binding.SendBT.isVisible = false
                            }
                            2 -> {
                                binding.buttonA.isVisible = true
                                binding.buttonB.isVisible = true
                                binding.buttonC.isVisible = true

                                binding.edittextAnswerthequestion.isVisible = false
                                binding.SendBT.isVisible = false
                            }
                            3 -> {
                                binding.buttonA.isVisible = true
                                binding.buttonB.isVisible = true
                                binding.buttonC.isVisible = true

                                binding.edittextAnswerthequestion.isVisible = false
                                binding.SendBT.isVisible = false
                            }
                            4 -> {
                                binding.buttonA.isVisible = false
                                binding.buttonB.isVisible = false
                                binding.buttonC.isVisible = false

                                binding.edittextAnswerthequestion.isVisible = true
                                binding.SendBT.isVisible = true
                            }
                            5 -> {
                                binding.buttonA.isVisible = true
                                binding.buttonB.isVisible = true
                                binding.buttonC.isVisible = true

                                binding.edittextAnswerthequestion.isVisible = false
                                binding.SendBT.isVisible = false
                            }
                            6 -> {
                                binding.buttonA.isVisible = false
                                binding.buttonB.isVisible = false
                                binding.buttonC.isVisible = false

                                binding.edittextAnswerthequestion.text.clear()
                                binding.edittextAnswerthequestion.isVisible = true
                                binding.SendBT.isVisible = true
                            }
                            7 -> {
                                binding.buttonA.isVisible = true
                                binding.buttonB.isVisible = true
                                binding.buttonC.isVisible = true

                                binding.edittextAnswerthequestion.isVisible = false
                                binding.SendBT.isVisible = false
                            }
                            8 -> {
                                binding.buttonA.isVisible = true
                                binding.buttonB.isVisible = true
                                binding.buttonC.isVisible = true

                                binding.edittextAnswerthequestion.isVisible = false
                                binding.SendBT.isVisible = false
                            }
                        }
                        if (turnNumber != 4 || turnNumber != 6)generateQuestion(category, turnNumber)
                        else if (turnNumber == 4 || turnNumber == 6)generateQuestionSecondOption(category, turnNumber)

                    }
                    if (binding.edittextAnswerthequestion.text.toString().uppercase() != result) {
                        points -= 1
                        println("Points $points")
                        turnNumber += 1
                        when (turnNumber){
                            1 -> {
                                binding.buttonA.isVisible = true
                                binding.buttonB.isVisible = true
                                binding.buttonC.isVisible = true

                                binding.edittextAnswerthequestion.isVisible = false
                                binding.SendBT.isVisible = false
                            }
                            2 -> {
                                binding.buttonA.isVisible = true
                                binding.buttonB.isVisible = true
                                binding.buttonC.isVisible = true

                                binding.edittextAnswerthequestion.isVisible = false
                                binding.SendBT.isVisible = false
                            }
                            3 -> {
                                binding.buttonA.isVisible = true
                                binding.buttonB.isVisible = true
                                binding.buttonC.isVisible = true

                                binding.edittextAnswerthequestion.isVisible = false
                                binding.SendBT.isVisible = false
                            }
                            4 -> {
                                binding.buttonA.isVisible = false
                                binding.buttonB.isVisible = false
                                binding.buttonC.isVisible = false

                                binding.edittextAnswerthequestion.isVisible = true
                                binding.SendBT.isVisible = true
                            }
                            5 -> {
                                binding.buttonA.isVisible = true
                                binding.buttonB.isVisible = true
                                binding.buttonC.isVisible = true

                                binding.edittextAnswerthequestion.isVisible = false
                                binding.SendBT.isVisible = false
                            }
                            6 -> {
                                binding.buttonA.isVisible = false
                                binding.buttonB.isVisible = false
                                binding.buttonC.isVisible = false

                                binding.edittextAnswerthequestion.text.clear()
                                binding.edittextAnswerthequestion.isVisible = true
                                binding.SendBT.isVisible = true
                            }
                            7 -> {
                                binding.buttonA.isVisible = true
                                binding.buttonB.isVisible = true
                                binding.buttonC.isVisible = true

                                binding.edittextAnswerthequestion.isVisible = false
                                binding.SendBT.isVisible = false
                            }
                            8 -> {
                                binding.buttonA.isVisible = true
                                binding.buttonB.isVisible = true
                                binding.buttonC.isVisible = true

                                binding.edittextAnswerthequestion.isVisible = false
                                binding.SendBT.isVisible = false
                            }
                        }
                        if (turnNumber != 4 || turnNumber != 6)generateQuestion(category, turnNumber)
                        else if (turnNumber == 4 || turnNumber == 6)generateQuestionSecondOption(category, turnNumber)
                    }
                    true
                } else false
            }

            binding.SendBT.setOnClickListener{
                if (binding.edittextAnswerthequestion.text.toString().uppercase() == result) {
                    points += 10
                    println("Points $points")
                    turnNumber += 1
                    when (turnNumber){
                        1 -> {
                            binding.buttonA.isVisible = true
                            binding.buttonB.isVisible = true
                            binding.buttonC.isVisible = true

                            binding.edittextAnswerthequestion.isVisible = false
                            binding.SendBT.isVisible = false
                        }
                        2 -> {
                            binding.buttonA.isVisible = true
                            binding.buttonB.isVisible = true
                            binding.buttonC.isVisible = true

                            binding.edittextAnswerthequestion.isVisible = false
                            binding.SendBT.isVisible = false
                        }
                        3 -> {
                            binding.buttonA.isVisible = true
                            binding.buttonB.isVisible = true
                            binding.buttonC.isVisible = true

                            binding.edittextAnswerthequestion.isVisible = false
                            binding.SendBT.isVisible = false
                        }
                        4 -> {
                            binding.buttonA.isVisible = false
                            binding.buttonB.isVisible = false
                            binding.buttonC.isVisible = false

                            binding.edittextAnswerthequestion.isVisible = true
                            binding.SendBT.isVisible = true
                        }
                        5 -> {
                            binding.buttonA.isVisible = true
                            binding.buttonB.isVisible = true
                            binding.buttonC.isVisible = true

                            binding.edittextAnswerthequestion.isVisible = false
                            binding.SendBT.isVisible = false
                        }
                        6 -> {
                            binding.buttonA.isVisible = false
                            binding.buttonB.isVisible = false
                            binding.buttonC.isVisible = false

                            binding.edittextAnswerthequestion.text.clear()
                            binding.edittextAnswerthequestion.isVisible = true
                            binding.SendBT.isVisible = true
                        }
                        7 -> {
                            binding.buttonA.isVisible = true
                            binding.buttonB.isVisible = true
                            binding.buttonC.isVisible = true

                            binding.edittextAnswerthequestion.isVisible = false
                            binding.SendBT.isVisible = false
                        }
                        8 -> {
                            binding.buttonA.isVisible = true
                            binding.buttonB.isVisible = true
                            binding.buttonC.isVisible = true

                            binding.edittextAnswerthequestion.isVisible = false
                            binding.SendBT.isVisible = false
                        }
                    }
                    if (turnNumber != 4 || turnNumber != 6)generateQuestion(category, turnNumber)
                    else if (turnNumber == 4 || turnNumber == 6)generateQuestionSecondOption(category, turnNumber)

                }
                if (binding.edittextAnswerthequestion.text.toString().uppercase() != result) {
                    points -= 1
                    println("Points $points")
                    turnNumber += 1
                    when (turnNumber){
                        1 -> {
                            binding.buttonA.isVisible = true
                            binding.buttonB.isVisible = true
                            binding.buttonC.isVisible = true

                            binding.edittextAnswerthequestion.isVisible = false
                            binding.SendBT.isVisible = false
                        }
                        2 -> {
                            binding.buttonA.isVisible = true
                            binding.buttonB.isVisible = true
                            binding.buttonC.isVisible = true

                            binding.edittextAnswerthequestion.isVisible = false
                            binding.SendBT.isVisible = false
                        }
                        3 -> {
                            binding.buttonA.isVisible = true
                            binding.buttonB.isVisible = true
                            binding.buttonC.isVisible = true

                            binding.edittextAnswerthequestion.isVisible = false
                            binding.SendBT.isVisible = false
                        }
                        4 -> {
                            binding.buttonA.isVisible = false
                            binding.buttonB.isVisible = false
                            binding.buttonC.isVisible = false

                            binding.edittextAnswerthequestion.isVisible = true
                            binding.SendBT.isVisible = true
                        }
                        5 -> {
                            binding.buttonA.isVisible = true
                            binding.buttonB.isVisible = true
                            binding.buttonC.isVisible = true

                            binding.edittextAnswerthequestion.isVisible = false
                            binding.SendBT.isVisible = false
                        }
                        6 -> {
                            binding.buttonA.isVisible = false
                            binding.buttonB.isVisible = false
                            binding.buttonC.isVisible = false

                            binding.edittextAnswerthequestion.text.clear()
                            binding.edittextAnswerthequestion.isVisible = true
                            binding.SendBT.isVisible = true
                        }
                        7 -> {
                            binding.buttonA.isVisible = true
                            binding.buttonB.isVisible = true
                            binding.buttonC.isVisible = true

                            binding.edittextAnswerthequestion.isVisible = false
                            binding.SendBT.isVisible = false
                        }
                        8 -> {
                            binding.buttonA.isVisible = true
                            binding.buttonB.isVisible = true
                            binding.buttonC.isVisible = true

                            binding.edittextAnswerthequestion.isVisible = false
                            binding.SendBT.isVisible = false
                        }
                    }
                    if (turnNumber != 4 || turnNumber != 6)generateQuestion(category, turnNumber)
                    else if (turnNumber == 4 || turnNumber == 6)generateQuestionSecondOption(category, turnNumber)
                }
            }

            if (turnNumber != 8){
                Handler().postDelayed({

                    when (turnNumber){
                        1 -> {
                            binding.buttonA.isVisible = true
                            binding.buttonB.isVisible = true
                            binding.buttonC.isVisible = true

                            binding.edittextAnswerthequestion.isVisible = false
                            binding.SendBT.isVisible = false
                        }
                        2 -> {
                            binding.buttonA.isVisible = true
                            binding.buttonB.isVisible = true
                            binding.buttonC.isVisible = true

                            binding.edittextAnswerthequestion.isVisible = false
                            binding.SendBT.isVisible = false
                        }
                        3 -> {
                            binding.buttonA.isVisible = true
                            binding.buttonB.isVisible = true
                            binding.buttonC.isVisible = true

                            binding.edittextAnswerthequestion.isVisible = false
                            binding.SendBT.isVisible = false
                        }
                        4 -> {
                            binding.buttonA.isVisible = false
                            binding.buttonB.isVisible = false
                            binding.buttonC.isVisible = false

                            binding.edittextAnswerthequestion.isVisible = true
                            binding.SendBT.isVisible = true
                        }
                        5 -> {
                            binding.buttonA.isVisible = true
                            binding.buttonB.isVisible = true
                            binding.buttonC.isVisible = true

                            binding.edittextAnswerthequestion.isVisible = false
                            binding.SendBT.isVisible = false
                        }
                        6 -> {
                            binding.buttonA.isVisible = false
                            binding.buttonB.isVisible = false
                            binding.buttonC.isVisible = false

                            binding.edittextAnswerthequestion.text.clear()
                            binding.edittextAnswerthequestion.isVisible = true
                            binding.SendBT.isVisible = true
                        }
                        7 -> {
                            binding.buttonA.isVisible = true
                            binding.buttonB.isVisible = true
                            binding.buttonC.isVisible = true

                            binding.edittextAnswerthequestion.isVisible = false
                            binding.SendBT.isVisible = false
                        }
                        8 -> {
                            binding.buttonA.isVisible = true
                            binding.buttonB.isVisible = true
                            binding.buttonC.isVisible = true

                            binding.edittextAnswerthequestion.isVisible = false
                            binding.SendBT.isVisible = false
                        }
                    }
                    if (turnNumber != 4 || turnNumber != 6)generateQuestion(category, turnNumber)
                    else if (turnNumber == 4 || turnNumber == 6)generateQuestionSecondOption(category, turnNumber)

                    binding.buttonA.isClickable = true
                    binding.buttonB.isClickable = true
                    binding.buttonC.isClickable = true
                    binding.buttonA.alpha = 1f
                    binding.buttonB.alpha = 1f
                    binding.buttonC.alpha = 1f
                    button.background.alpha = 255
                    button.isEnabled = true
                    binding.buttonA.setBackgroundColor(resources.getColor(R.color.aquamarine))
                    binding.buttonB.setBackgroundColor(resources.getColor(R.color.aquamarine))
                    binding.buttonC.setBackgroundColor(resources.getColor(R.color.aquamarine))
                    binding.progressbarr.progress = 20
                    binding.progressSeconds.text = (" ${binding.progressbarr.progress.toString()} s")
                    timer.start()

                } , 1000)
            }else{
                timer.cancel()
                writeFile(points)
                Handler().postDelayed({
                    parentFragmentManager.beginTransaction().apply {
                        replace(R.id.fragmentContainerView, (FragmentGame()))
                        setReorderingAllowed(true)
                        commit()
                    }
                },2000)
            }
    }

    fun generateQuestion(category: String, turnNumber: Int){
        when (category){
            "Historia" -> {
                if (Options.options[0+turnNumber].category == "Historia" ){
                    binding.textView.text = Options.options[0+turnNumber].question
                    binding.buttonA.text = Options.options[0+turnNumber].possibleOption1
                    binding.buttonB.text = Options.options[0+turnNumber].possibleOption2
                    binding.buttonC.text = Options.options[0+turnNumber].possibleOption3
                    result = Options.options[0+turnNumber].correctOption
                }
            }
            "Esports" -> {
                if (Options.options[8+turnNumber].category == "Esports" ){
                    binding.textView.text = Options.options[8+turnNumber].question
                    binding.buttonA.text = Options.options[8+turnNumber].possibleOption1
                    binding.buttonB.text = Options.options[8+turnNumber].possibleOption2
                    binding.buttonC.text = Options.options[8+turnNumber].possibleOption3
                    result = Options.options[8+turnNumber].correctOption
                }
            }
            "Entreteniment" -> {
                if (Options.options[16+turnNumber].category == "Entreteniment" ){
                    binding.textView.text = Options.options[16+turnNumber].question
                    binding.buttonA.text = Options.options[16+turnNumber].possibleOption1
                    binding.buttonB.text = Options.options[16+turnNumber].possibleOption2
                    binding.buttonC.text = Options.options[16+turnNumber].possibleOption3
                    result = Options.options[16+turnNumber].correctOption
                }
            }
            "Geografia" -> {
                if (Options.options[24+turnNumber].category == "Geografia" ){
                    binding.textView.text = Options.options[24+turnNumber].question
                    binding.buttonA.text = Options.options[24+turnNumber].possibleOption1
                    binding.buttonB.text = Options.options[24+turnNumber].possibleOption2
                    binding.buttonC.text = Options.options[24+turnNumber].possibleOption3
                    result = Options.options[randomQuestion+24].correctOption
                }
            }
            "Ciencia" -> {
                if (Options.options[32+turnNumber].category == "Ciencia" ){
                    binding.textView.text = Options.options[32+turnNumber].question
                    binding.buttonA.text = Options.options[32+turnNumber].possibleOption1
                    binding.buttonB.text = Options.options[32+turnNumber].possibleOption2
                    binding.buttonC.text = Options.options[32+turnNumber].possibleOption3
                    result = Options.options[randomQuestion+32].correctOption
                }
            }
            "Art" -> {
                if (Options.options[39+turnNumber].category == "Art" ){
                    binding.textView.text = Options.options[39+turnNumber].question
                    binding.buttonA.text = Options.options[39+turnNumber].possibleOption1
                    binding.buttonB.text = Options.options[39+turnNumber].possibleOption2
                    binding.buttonC.text = Options.options[39+turnNumber].possibleOption3
                    result = Options.options[39+turnNumber].correctOption
                }
            }

        }
    }

    fun generateQuestionSecondOption(category: String, turnNumber: Int){
        when (category){
            "Historia" -> {
                if (Questions.questions[randomQuestion].category == "Historia" ){
                    binding.textView.text = Questions.questions[0+turnNumber].question
                    result = Questions.questions[0+turnNumber].correctAnswer.uppercase()
                }
            }
            "Esports" -> {
                if (Questions.questions[randomQuestion+8].category == "Esports" ){
                    binding.textView.text = Questions.questions[8+turnNumber].question
                    result = Questions.questions[8+turnNumber].correctAnswer.uppercase()
                }
            }
            "Entreteniment" -> {
                if (Questions.questions[randomQuestion+16].category == "Entreteniment" ){
                    binding.textView.text = Questions.questions[16+turnNumber].question
                    result = Questions.questions[16+turnNumber].correctAnswer.uppercase()
                }
            }
            "Geografia" -> {
                if (Questions.questions[randomQuestion+24].category == "Geografia" ){
                    binding.textView.text = Questions.questions[24+turnNumber].question
                    result = Questions.questions[24+turnNumber].correctAnswer.uppercase()
                }
            }
            "Ciencia" -> {
                if (Questions.questions[randomQuestion+32].category == "Ciencia" ){
                    binding.textView.text = Questions.questions[32+turnNumber].question
                    result = Questions.questions[32+turnNumber].correctAnswer.uppercase()
                }
            }
            "Art" -> {
                if (Questions.questions[randomQuestion+38].category == "Art" ){
                    binding.textView.text = Questions.questions[40+turnNumber].question
                    result = Questions.questions[40+turnNumber].correctAnswer.uppercase()
                }
            }
        }
    }

    fun writeFile(points:Int){
        val file = OutputStreamWriter(requireActivity().openFileOutput("DataUser", Activity.MODE_APPEND))
        file.append("$points\n")
        file.flush()
        file.close()
    }
}