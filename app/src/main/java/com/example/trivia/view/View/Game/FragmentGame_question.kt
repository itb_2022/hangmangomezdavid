package com.example.trivia.view.View.Game

import android.os.Bundle
import android.os.Handler
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.trivia.R
import com.example.trivia.databinding.FragmentGameQuestionBinding
import com.example.trivia.model.Questions

class FragmentGame_question : Fragment() {
    lateinit var binding: FragmentGameQuestionBinding
    var randomQuestion = 0
    var category = ""
    var result = ""
    var points = 0
    var turnNumber = 0
    var rangeFragment = 0



    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        binding = FragmentGameQuestionBinding.inflate(layoutInflater)
        return binding.root
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        parentFragmentManager.setFragmentResultListener("userData", this,
            { requestKey: String, result: Bundle ->
                category = result.getString("category").toString()
                points = result.getInt("points")
                turnNumber = result.getInt("turnNumber")
            })

        Handler().postDelayed({
            println(category)
            binding.categoryTextview.text = category
            randomQuestion = (0..7).random()
            println(randomQuestion)
            when (category){
                "Historia" -> {
                    if (Questions.questions[randomQuestion].category == "Historia" ){
                        binding.questionTextview.text = Questions.questions[randomQuestion].question
                        result = Questions.questions[randomQuestion].correctAnswer
                    }
                }
                "Esports" -> {
                    if (Questions.questions[randomQuestion+8].category == "Esports" ){
                        binding.questionTextview.text = Questions.questions[randomQuestion+7].question
                        result = Questions.questions[randomQuestion+8].correctAnswer
                    }
                }
                "Entreteniment" -> {
                    if (Questions.questions[randomQuestion+16].category == "Entreteniment" ){
                        binding.questionTextview.text = Questions.questions[randomQuestion+15].question
                        result = Questions.questions[randomQuestion+16].correctAnswer
                    }
                }
                "Geografia" -> {
                    if (Questions.questions[randomQuestion+24].category == "Geografia" ){
                        binding.questionTextview.text = Questions.questions[randomQuestion+23].question
                        result = Questions.questions[randomQuestion+24].correctAnswer
                    }
                }
                "Ciencia" -> {
                    if (Questions.questions[randomQuestion+32].category == "Ciencia" ){
                        binding.questionTextview.text = Questions.questions[randomQuestion+31].question
                        result = Questions.questions[randomQuestion+32].correctAnswer
                    }
                }
                "Art" -> {
                    if (Questions.questions[randomQuestion+38].category == "Art" ){
                        binding.questionTextview.text = Questions.questions[randomQuestion+38].question
                        result = Questions.questions[randomQuestion+38].correctAnswer
                    }
                }
            }
        } , 0)

        binding.edittextAnswerthequestion.setOnKeyListener { view, keyCode, keyevent -> //If the keyevent is a key-down event on the "enter" button
            println(keyCode)
            println(keyevent.action)
            if (keyevent.action == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
                println(keyCode)
                println(keyevent.action)
                if (binding.edittextAnswerthequestion.text.toString() == result) {
                    points += 10
                    println("Points $points")

                }
                if (binding.questionTextview.text.toString() != result) {
                    points -= 1
                    println("Points $points")
                }
                turnNumber += 1
                true
            } else false
        }

    }

}