package com.example.trivia.view.View.Help

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.trivia.view.View.Menu.FragmentMenu
import com.example.trivia.R
import com.example.trivia.databinding.FragmentHelpBinding


class FragmentHelp : Fragment() {
    lateinit var binding: FragmentHelpBinding
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = FragmentHelpBinding.inflate(layoutInflater)
        return binding.root
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.ReturnBT.setOnClickListener {

            parentFragmentManager.beginTransaction().apply {
                replace(R.id.fragmentContainerView, FragmentMenu())
                setReorderingAllowed(true)
                addToBackStack("Help")
                commit()
            }
        }
        binding.section1.setOnClickListener {
            parentFragmentManager.beginTransaction().apply {
                replace(R.id.fragmentContainerView, FragmentHelp_about())
                setReorderingAllowed(true)
                addToBackStack("Help")
                commit()
            }
        }
        binding.section2.setOnClickListener {
            parentFragmentManager.beginTransaction().apply {
                replace(R.id.fragmentContainerView, FragmentHelp_How())
                setReorderingAllowed(true)
                addToBackStack("Help")
                commit()
            }
        }
        binding.section3.setOnClickListener {
            parentFragmentManager.beginTransaction().apply {
                replace(R.id.fragmentContainerView, FragmentHelp_Ranking())
                setReorderingAllowed(true)
                addToBackStack("Help")
                commit()
            }
        }
        binding.section4.setOnClickListener {
            parentFragmentManager.beginTransaction().apply {
                replace(R.id.fragmentContainerView, FragmentHelp_AboutUs())
                setReorderingAllowed(true)
                addToBackStack("Help")
                commit()
            }
        }
    }
}