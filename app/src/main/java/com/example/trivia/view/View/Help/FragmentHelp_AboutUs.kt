package com.example.trivia.view.View.Help

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.trivia.R
import com.example.trivia.databinding.FragmentHelpAboutUsBinding


class FragmentHelp_AboutUs : Fragment() {
    lateinit var binding: FragmentHelpAboutUsBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = FragmentHelpAboutUsBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.ReturnBT.setOnClickListener {

            parentFragmentManager.beginTransaction().apply {
                replace(R.id.fragmentContainerView, FragmentHelp())
                setReorderingAllowed(true)
                addToBackStack("Help")
                commit()
            }
        }
    }
}