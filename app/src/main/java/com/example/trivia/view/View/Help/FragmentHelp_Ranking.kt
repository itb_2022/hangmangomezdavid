package com.example.trivia.view.View.Help

import android.app.Activity
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.trivia.R
import com.example.trivia.databinding.FragmentHelpRankingBinding
import java.io.BufferedReader
import java.io.InputStreamReader
import java.io.OutputStreamWriter


class FragmentHelp_Ranking : Fragment() {
    lateinit var binding: FragmentHelpRankingBinding
    
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = FragmentHelpRankingBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.ReturnBT.setOnClickListener {

            parentFragmentManager.beginTransaction().apply {
                replace(R.id.fragmentContainerView, FragmentHelp())
                setReorderingAllowed(true)
                addToBackStack("Help")
                commit()
            }

        }
    }

}