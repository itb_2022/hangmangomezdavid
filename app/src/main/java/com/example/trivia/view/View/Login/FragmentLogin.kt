package com.example.trivia.view.View.Login

import android.app.Activity
import android.os.Bundle
import android.service.autofill.UserData
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.trivia.view.View.Menu.FragmentMenu
import com.example.trivia.R
import java.io.File
import com.example.trivia.databinding.FragmentLoginBinding
import com.example.trivia.model.User
import com.example.trivia.model.Users
import java.io.BufferedReader
import java.io.InputStreamReader
import java.io.OutputStreamWriter
import kotlin.math.floor


class FragmentLogin : Fragment() {
    lateinit var binding: FragmentLoginBinding
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = FragmentLoginBinding.inflate(layoutInflater)
        return binding.root
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        binding.loginBt.setOnClickListener {

            if (binding.usernameInput.text.isEmpty()==true || binding.pass.text.isEmpty()==true){
                Toast.makeText(requireContext(),"Empty fields", Toast.LENGTH_SHORT).show()
            }

            if (binding.usernameInput.text.isEmpty()==false && binding.pass.text.isEmpty()==false){
                //val randomHash = "0x"+"${floor(100000 + Math.random() * 900000)}"
                val nameUserLogin = binding.usernameInput.text.toString()
                val passUserLogin = binding.pass.text.toString()
                writeFile(nameUserLogin,passUserLogin)

                parentFragmentManager.beginTransaction().apply {
                    replace(R.id.fragmentContainerView, FragmentMenu())
                    setReorderingAllowed(true)
                    commit()
                }
            }

        }


    }
    fun readFile() {
        var content = ""
        val file = InputStreamReader(requireActivity().openFileInput("DataUser"))
        val br = BufferedReader(file)
        var line = br.readLine()
        while(line!=null){
            content += line + "\n"
            line = br.readLine()
        }
    }
    fun writeFile(nameUserLogin:String,passUserLogin:String){
        val file = OutputStreamWriter(requireActivity().openFileOutput("DataUser", Activity.MODE_APPEND))
        file.append("$nameUserLogin,$passUserLogin")
        file.flush()
        file.close()
    }

}