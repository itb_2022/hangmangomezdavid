package com.example.trivia.view.View

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.trivia.view.View.Login.FragmentLogin
import com.example.trivia.R
import com.example.trivia.view.View.Menu.FragmentMenu

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        supportFragmentManager.beginTransaction().apply {
            replace(R.id.fragmentContainerView, FragmentMenu())
            setReorderingAllowed(true)
            addToBackStack("name") // name can be null
            commit()
        }
    }
}