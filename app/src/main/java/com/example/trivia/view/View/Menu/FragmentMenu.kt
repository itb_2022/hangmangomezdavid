package com.example.trivia.view.View.Menu

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.RotateAnimation
import androidx.fragment.app.Fragment
import com.example.trivia.view.View.Game.FragmentGame
import com.example.trivia.R
import com.example.trivia.databinding.FragmentMenuBinding
import com.example.trivia.model.User
import com.example.trivia.view.View.Help.FragmentHelp
import com.example.trivia.view.View.Ranking.FragmentRanking
import java.io.BufferedReader
import java.io.InputStreamReader


class FragmentMenu : Fragment() {
    lateinit var binding: FragmentMenuBinding
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        binding = FragmentMenuBinding.inflate(layoutInflater)
        return binding.root
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val rotate = RotateAnimation(0f, 360f,
            Animation.RELATIVE_TO_SELF, 0.5f,
            Animation.RELATIVE_TO_SELF, 0.5f
        )
        rotate.duration = 2000
        rotate.repeatCount = Animation.INFINITE
        binding.roulette.startAnimation(rotate)

        binding.PlayBT.setOnClickListener {

            parentFragmentManager.beginTransaction().apply {
                replace(R.id.fragmentContainerView, FragmentGame())
                setReorderingAllowed(true)
                commit()
            }
        }

        binding.RankingBT.setOnClickListener {

            parentFragmentManager.beginTransaction().apply {
                replace(R.id.fragmentContainerView, FragmentRanking())
                setReorderingAllowed(true)
                addToBackStack("Menu")
                commit()
            }
        }
        binding.HelpBT.setOnClickListener {

            parentFragmentManager.beginTransaction().apply {
                replace(R.id.fragmentContainerView, FragmentHelp())
                setReorderingAllowed(true)
                addToBackStack("Menu")
                commit()
            }
        }
    }

}