package com.example.trivia.view.View.Ranking

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import com.example.trivia.view.View.Menu.FragmentMenu
import com.example.trivia.R
import com.example.trivia.databinding.FragmentRankingBinding
import com.example.trivia.model.User
import java.io.BufferedReader
import java.io.InputStreamReader

class FragmentRanking : Fragment() {
    lateinit var binding: FragmentRankingBinding
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        binding = FragmentRankingBinding.inflate(layoutInflater)
        return binding.root
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        var ListOfUsers = readFile()
        var sizeofusers = ListOfUsers.size

        if (ListOfUsers.isEmpty()){
            Toast.makeText(requireContext(),"Aun no has jugado, crack!", Toast.LENGTH_SHORT).show()
        }
        else {
            when(sizeofusers){
            1 -> {
                binding.UsernameRank1.isVisible = true
                binding.points1.isVisible = true
                binding.rankposition1.isVisible = true

                binding.UsernameRank1.text = ListOfUsers[0].userName
                binding.UsernameRank1.text = ListOfUsers[0].points.toString()
            }
            2 -> {
                binding.UsernameRank1.isVisible = true
                binding.points1.isVisible = true
                binding.rankposition1.isVisible = true

                binding.UsernameRank1.text = ListOfUsers[1].userName
                binding.UsernameRank1.text = ListOfUsers[1].points.toString()
            }
            3 -> {
                binding.UsernameRank1.isVisible = true
                binding.points1.isVisible = true
                binding.rankposition1.isVisible = true

                binding.UsernameRank1.text = ListOfUsers[2].userName
                binding.UsernameRank1.text = ListOfUsers[2].points.toString()
            }
            4 -> {
                binding.UsernameRank1.isVisible = true
                binding.points1.isVisible = true
                binding.rankposition1.isVisible = true

                binding.UsernameRank1.text = ListOfUsers[3].userName
                binding.UsernameRank1.text = ListOfUsers[3].points.toString()
            }
        }}

        binding.ReturnBT.setOnClickListener {

            parentFragmentManager.beginTransaction().apply {
                replace(R.id.fragmentContainerView, FragmentMenu())
                setReorderingAllowed(true)
                addToBackStack("Ranking")
                commit()
            }
        }

        binding.ShareBT.setOnClickListener {
            binding.ShareBT.setOnClickListener {
                val intent = Intent(Intent.ACTION_SEND)
                intent.putExtra(Intent.EXTRA_TEXT, "${ListOfUsers[0].userName} ha obtenido una puntuación de: ${ListOfUsers[0].points}\" , \n${ListOfUsers[1].userName} ha obtenido una puntuación de: ${ListOfUsers[1].points}\" , \n${ListOfUsers[2].userName} ha obtenido una puntuación de: ${ListOfUsers[2].points}\" , \n${ListOfUsers[3].userName} ha obtenido una puntuación de: ${ListOfUsers[3].points}")
                intent.type = "text/plain"
                startActivity(intent)
            }
        }

    }
    fun readFile():List<User> {
        var content = mutableListOf<String>()
        var users = mutableListOf<User>()
        val file = InputStreamReader(requireActivity().openFileInput("DataUser"))
        val br = BufferedReader(file)
        var line = br.readLine()
        while(line!=null){
            content += line + "\n"
            line = br.readLine()
        }

        for (i in content){
            var temp = i.split(",")
            if (temp.size > 2){
                var user = User(temp[0],temp[1],temp[2].toInt() )
                users.add(user)
            }
        }
        return users.sortedByDescending { it.points }
    }
}